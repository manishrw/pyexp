import os
import hashlib
import urllib2 as ul
import sys

def get_hash(name):
	readsize = 64 * 1024
	with open(name, 'rb') as f:		            
	        data = f.read(readsize)
	        f.seek(-readsize, os.SEEK_END)
                data += f.read(readsize)
        return hashlib.md5(data).hexdigest()

def download_subtitles(path):
	formats = ['.avi', '.mkv', 'flv', '.mp4', '.mpg', 'mpeg4', '.qt', '.rm']
	for format in formats:
		path_s = path.replace(format, '')
	path_s += '.srt'
	if not os.path.exists(path_s):
		print path, path_s, get_hash(path)
		header = { 'User-Agent' : 'SubDB/1.0 (subtitle_down/1.0; https://bitbucket.org/manishrw/pyexp)' }
		url = "http://api.thesubdb.com/?action=download&hash="+get_hash(path)+"&language=en"
		req = ul.Request(url, '', header)
		response = ul.urlopen(req).read()
		subs = open(path_s, 'wb')
		subs.write(response)

if __name__ == '__main__':
	download_subtitles(sys.argv[1])
